import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
      home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
              title: Text("Aplikasi Saya"),
              backgroundColor: Colors.blue,
              leading: Icon(
                Icons.menu,
                color: Colors.white,
                size: 30,
              ),
              actions: <Widget>[
                IconButton(
                    icon: Icon(Icons.thumb_up, color: Colors.white),
                    onPressed: null),
                IconButton(
                    icon: Icon(Icons.thumb_down, color: Colors.white),
                    onPressed: null)
              ]),
          body: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.network("https://i.imgur.com/uo8KFWQ.jpg"),
                Padding(padding: EdgeInsets.only(top: 20.0)),
                Text(
                  "Robert Downey Jr",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 20.0),
                ),
              ],
            ),
          ))));
}
